//
//  SphereNode.swift
//  ARScale
//
//  Created by Bivas Biswas on 6/14/18.
//  Copyright © 2018 Bivas Biswas. All rights reserved.
//

import SceneKit

class SphereNode : SCNNode{
    
    init(position p: SCNVector3){
        super.init()
        let sphereGeo = SCNSphere(radius: 0.005)
        self.geometry = sphereGeo
        self.position = p
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
