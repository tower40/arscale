//
//  ViewController.swift
//  ARScale
//
//  Created by Bivas Biswas on 6/14/18.
//  Copyright © 2018 Bivas Biswas. All rights reserved.
//

import UIKit
import ARKit
import SceneKit

class ViewController: UIViewController, ARSCNViewDelegate {
    
    lazy var sceneView: ARSCNView = {
        let view = ARSCNView(frame: CGRect.zero)
        view.delegate = self
        return view
    }()
    
    lazy var infoLabel: UILabel = {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.title1)
        label.textAlignment = .center
        label.backgroundColor = .white
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.addSubview(sceneView)
        view.addSubview(infoLabel)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(OnTap))
        tapRecognizer.numberOfTapsRequired = 1;
        sceneView.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func OnTap(sender: UITapGestureRecognizer){
        
        let touchPoint = sender.location(in: sceneView)
        
        let hitTestResults = sceneView.hitTest(touchPoint, types: .featurePoint)
        if let result = hitTestResults.last{
        
            // Creates an SCNVector3 with certain indexes in the matrix
            let pos = SCNVector3.positionFrom(matrix: result.worldTransform)
            
            let sphere = SphereNode(position: pos)
          
            sceneView.scene.rootNode.addChildNode(sphere)
        }
    }
    
   
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sceneView.frame = view.bounds
        infoLabel.frame = CGRect(x:0, y:16, width: view.bounds.width, height: 64)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration, options : [.resetTracking, .removeExistingAnchors])
    }
    
    func session(_ session:ARSession, cameraDidChangeTrackingState param: ARCamera){
        var status = "Loading..."
        switch(param.trackingState){
        case .notAvailable:
            status = "Not Available"
            fallthrough
        case .limited(_):
            status = "Analyzing"
        case .normal:
            status = "Ready"
        }
        
        infoLabel.text = status;
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        infoLabel.text = "Session Was Interrupted"
    }
    
    
}

extension SCNVector3 {
    
    static func positionFrom(matrix: matrix_float4x4) -> SCNVector3{
        let column = matrix.columns.3
        return SCNVector3(column.x, column.y, column.z)
    }
    
}


